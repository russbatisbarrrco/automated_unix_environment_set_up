# Environment set up automation

This program helps quickly set up an environment after installing an OS.
The way better use of this is program is to customized it to add the everyday sofware you use into the 'set_up' scripts, so that next time you need to format or buy a new computer you can do it very quickly, without finfing the commands from scratch again. 
Also this program provides productuvity tools that help you save time and energy on everyday tasks, like the shell enhacements.

### *Basic install:* 

[Curl](https://curl.se) - [Git](https://git-scm.com/book/en/v2/Getting-Started-Installing-Git) - [Xclip](https://github.com/astrand/xclip) - [Pip](https://pip.pypa.io/en/stable/installation/) - [Snap and Snap-store](https://snapcraft.io/docs/installing-snap-on-linux-mint) - [NVM](https://github.com/nvm-sh/nvm) - [node](https://nodejs.org/) - [Yarn](https://classic.yarnpkg.com/lang/en/docs/install/#debian-stable) - [Telegram](https://snapcraft.io/telegram-desktop) - [VSCode](https://code.visualstudio.com/docs/setup/linux) - [Vim](https://www.vim.org/) - [Neovim](https://neovim.io/) - [Postman](https://learning.postman.com/docs/getting-started/installation-and-updates/#installing-postman-on-linux
) - [RUST](https://www.rust-lang.org/tools/install)

### *Full (prod):* 

Basic install + [Notion](https://snapcraft.io/notion-snap) - [tmux](https://github.com/tmux/tmux/wiki) - [Powerline fonts](https://github.com/powerline/fonts) - [vim-plug](https://github.com/junegunn/vim-plug)

Plus a terminal enhancement, either from [enhancing Bash](https://github.com/ohmybash/oh-my-bash) or from installing and setting up [zsh](https://www.zsh.org/), ([Oh My Zsh](https://ohmyz.sh/) and [zsh-atosuggestions](https://github.com/zsh-users/zsh-autosuggestions/blob/master/INSTALL.md))


### Advice to anyone reading:
I'm using my personal configuration and set up, please navigate to these files and cutomize them to what you need:
* `/my_unix_scripts/linux_setup/set_ups/utils/utils.sh`
* `/my_unix_scripts/linux_setup/set_ups/productivity/productivity/productivity.sh`
* `/my_unix_scripts/linux_setup/set_ups/set_ups/abbreviations/aliases.sh`


Add any [configs](https://github.com/Rbatistab/dotfiles/) you could need, like `.vimrc` config, docker, intelliJ or whatever is best for you to install in as many manual steps as possible :)


## Basic install:

1. Go to `linux_setup` directory and run:
  ```
  chmod +x install.sh
  ./install.sh
  ```

## Full install(includes productivity system):

1. Go to `linux_setup` directory and run:
  ```
  chmod +x install.sh
  ./install.sh -f
  ```
2. If you selected `zsh` as your preference terminal then also run:
  ```
  bash complete_zsh_setup.sh
  ```
   Once this script has been ran, visit https://github.com/romkatv/powerlevel10k/issues/310 and install the fonts manually as mentioned there.
   Then restart your shell.

3. Add any additional configuration files you need, example: https://github.com/Rbatistab/dotfiles/

## Post-install (productuvity choice)

1. Set up shortcut for [CopyQ](https://github.com/hluk/CopyQ). Open the app and in preferences check `Autostart`, then add a new shortcut to command `copyq toggle`, for example `alt ctr c`
1. Go `Ulauncher`'s preferences and check `Launch at login`
