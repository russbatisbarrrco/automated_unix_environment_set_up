#!/bin/bash

###############################################################################
#                              Abbreviations                                  #
###############################################################################

###############################################################################
# Adds the proper abbreviations according to the terminal installed
###############################################################################



###############################################################################
#                                 Functions                                   #
###############################################################################


###############################################################################
# Creates the proper form of the aliases to be added to either .bashrc or 
# .zshrc with format 'alias [ALIAS]="[COMMAND]"' from the associative array in 
# file aliases.sh in the same directory as this file.
# Globals:
#   aliases_list
#   DESTINATION_FILE
# Arguments:
#   None
# Retuns:
#   None
###############################################################################
add_bash_and_zsh_aliases(){ 
  echo -ne "\n# Usefull aliases as shortcuts\n" >> ${DESTINATION_FILE}
  for alias in "${!aliases_list[@]}"; do
    line="alias ${alias}='${aliases_list[$alias]}'" 
    add_line_if_not_present_in_file "${line}" "${DESTINATION_FILE}"
  done
}


###############################################################################
# Adds fish abbreviations
# Globals:
#   aliases_list
#   FISH_ABBREVIATIONS_FILE
# Arguments:
#   None
# Retuns:
#   None
###############################################################################
add_fish_abbreviations(){ 
  # call to fish_abbreviations.fish from fish shell
  byellow_text "[WARNING] Fish abbreviations currently not done"
}

###############################################################################
#                              Program excecution                             #
###############################################################################

# Getting directory information:
CURRENT_SCRIPT="${BASH_SOURCE}"
TARGET="$(readlink "$CURRENT_SCRIPT}")"
CURRENT_DIR="$(dirname "${CURRENT_SCRIPT}")/${TARGET}"
LIB_FILE="${CURRENT_DIR}../../../../utils/lib.sh"
echo "LIB_FILE=${LIB_FILE}"
ALIASES_FILE="${CURRENT_DIR}aliases.sh"

# Importing text library functions to format text and asseritions
source ${LIB_FILE}

# Importing aliases array
source ${ALIASES_FILE}

# Menu to determine to right path
DESTINATION_FILE=""
#while getopts "zbf" opt; do
while getopts "zb" opt; do
  case ${opt} in
    z )
      green_text "Installing zsh aliases..."
      DESTINATION_FILE="${HOME}/.zshrc"
      add_bash_and_zsh_aliases      
      command_assertion
      ;;
    b )
      green_text "Installing bash aliases..."
      DESTINATION_FILE="${HOME}/.bashrc"
      add_bash_and_zsh_aliases      
      command_assertion
      ;;
#    f )  [TODO] add fish abbreviations
#      green_text "Installing fish abbreviations..."
#      red_text "To be done, must add manually"
#      exit 1
#      ;;
    \? )
      red_text "Invalid option: -${OPTARG}" 1>&2
      exit 1
      ;;
  esac
done
