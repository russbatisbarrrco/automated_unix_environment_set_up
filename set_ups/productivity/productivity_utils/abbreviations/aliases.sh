#!/bin/bash

###############################################################################
# This file contains an associative array with aliases or abbreviations that 
# will be later used to create them according to the shell being used.
##############################################################################

declare -A aliases_list
aliases_list=(
  [armv]='sudo apt autoremove'
  [cd.]='code .'
  [gupd]='sudo apt-get update'
  [gupg]='sudo apt-get upgrade'
  [upd]='sudo apt update'
  [upg]='sudo apt upgrade'
  [sts]='git status'
  [pll]='git pull'
  [psh]='git push'
  [p3]='python3'
  [cmt]='git commit -m'
  [gadd]='git add'
  [ckt]='git checkout '
  [bckt]='git checkout -b '
  [rsts]='cd ~/Documents/sandboxes/Rust'
  [pyts]='cd ~/Documents/sandboxes/Python'
  [linup]='sudo apt-get update && sudo apt-get upgrade -y'
  [sdbx]='cd ~/Documents/sandbox'
  [ssh_add]='ssh-add ~/.ssh/hp_scripts_ed25519'
  [xclp]='xclip -sel clip'
)
