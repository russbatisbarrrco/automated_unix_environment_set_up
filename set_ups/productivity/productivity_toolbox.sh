#!/bin/bash

###############################################################################
#                           Productivity toolbox:                             #
###############################################################################

###############################################################################
# Tools to enhance productivity in your tools
###############################################################################

# vim-plug - https://github.com/junegunn/vim-plug
green_text "Installing vim-plug for Vim..."
curl -fLo ~/.vim/autoload/plug.vim --create-dirs \
    https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
command_assertion
green_text "Installing vim-plug for Neovim..."
sh -c 'curl -fLo "${XDG_DATA_HOME:-$HOME/.local/share}"/nvim/site/autoload/plug.vim --create-dirs \
       https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim'
command_assertion



