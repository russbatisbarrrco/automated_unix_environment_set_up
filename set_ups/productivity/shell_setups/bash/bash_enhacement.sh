#!/bin/bash

###############################################################################
#                               Bash enhacement                               #
###############################################################################

###############################################################################
# Makes bash terminal more friendly and productive 
###############################################################################

# Importing text library functions to format text and asseritions
source utils/lib.sh

# Oh-my-bash - https://github.com/ohmybash/oh-my-bash
green_text "Installing Oh-my-bash..."
bash -c "$(curl -fsSL https://raw.githubusercontent.com/ohmybash/oh-my-bash/master/tools/install.sh)"
command_assertion

###############################################################################
#                                   Aliases                                   #
###############################################################################
# Calling to functions that execute's the aliases addition, with flag 'b' for bash
execute_abbreviations_script b

source ~/.bashrc
