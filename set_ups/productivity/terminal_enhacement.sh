#!/bin/bash

###############################################################################
#                             Terminal Enhancement                            #
###############################################################################

###############################################################################
# Installs a powerfull terminall, terminator.
# Adds desirable features to the terminal according to the desired shell, like 
# autocompletion, git symbols and information, and a friendly UI to work with.
###############################################################################



###############################################################################
#                                 Functions                                   #
###############################################################################


###############################################################################
# Show a option menu to the uset with the shell options and delegates to the 
# proper function to handle the installation and set up
# Globals:
#   ZSH_SETUP_FILE    - File that setups zsh and oh-my-zsh
#   FISH_SETUP_FILE   - File that setups fish
#   BASH_ENHANCE_FILE - File with logic to enchance bash
# Arguments:
#   None
# Retuns:
#   None
###############################################################################
shell_options_menu(){
  PS3="Please select shell that best fits your preferences: "
  #select shell in $shells; do
  select shell; do
    case $shell in
      'Zsh')
        bash ${ZSH_SETUP_FILE}
        break
        ;;
      'Bash')
        bash ${BASH_ENHANCE_FILE}
        break
        ;;
      *)
        red_text "Invlid option. Please select only an option from 1 to $#."			
        ;;
      esac
  done
}



###############################################################################
#                             Program Execution                               #
###############################################################################

# Getting directory information:
CURRENT_SCRIPT="${BASH_SOURCE}"
TARGET="$(readlink "$CURRENT_SCRIPT}")"
CURRENT_DIR="$(dirname "${CURRENT_SCRIPT}")/${TARGET}"

# Files set up:
ZSH_SETUP_FILE="${CURRENT_DIR}shell_setups/zsh/zsh_shell.sh"
FISH_SETUP_FILE="${CURRENT_DIR}shell_setups/fish/fish_shell.sh"
BASH_ENHANCE_FILE="${CURRENT_DIR}shell_setups/bash/bash_enhacement.sh"

# Importing text library functions to format text and asseritions
source utils/lib.sh

# Installing tmux terminal
green_text "Installing tmux..."
yes | sudo apt install tmux
command_assertion
green_text "Creating '~/.tmux.conf'..."
echo 'set -g default-terminal screen-256color' >> ~/.tmux.conf
command_assertion 

# Adding tmux tpm - https://github.com/tmux-plugins/tpm
green_text "Installing tmux-plugins..."
git clone https://github.com/tmux-plugins/tpm ~/.tmux/plugins/tpm
command_assertion 

# Menu for select options of shell
shells=(
  'Zsh'
  'Bash'
)

# Call out to the shell options menu
echo -ne "\nDo you wish to install any of these shells?\n"
shell_options_menu "${shells[@]}"

