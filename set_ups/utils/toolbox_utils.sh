#!/bin/bash

###############################################################################
# Here are your everyday use software required to do your work, code editors,
# IDEs, languages, design software, etc.
###############################################################################


# Importing text library functions to format text and asseritions
source utils/lib.sh

# VScode - https://code.visualstudio.com/docs/setup/linux
green_text "Installing VSCode..."
sudo snap install --classic code
command_assertion

# IntelliJ comunity - https://www.jetbrains.com/help/idea/installation-guide.html#4e38c3ef
green_text "Installing IntelliJ comunity..."
sudo snap install intellij-idea-community --classic
command_assertion

# Vim - https://www.vim.org/
green_text "Installing vim..."
yes | sudo apt install vim
command_assertion
green_text "Creating '~/.vimrc'..."
echo "Visit https://github.com/Rbatistab/my_configs/blob/main/vim/.vimrc" >> ~/.vimrc

# NVIM - https://github.com/neovim/neovim/wiki/Installing-Neovim
green_text "Installing nvim..."
yes | sudo apt install neovim
command_assertion
green_text "Creating '~/.config/nvim/init.vim'..."
mkdir -p ~/.config/nvim
echo "Visit https://github.com/Rbatistab/my_configs/tree/main/nvim" >> ~/.config/nvim/init.vim

# Postman - https://learning.postman.com/docs/getting-started/installation-and-updates/#installing-postman-on-linux
green_text "Installing Postman..."
yes | sudo snap install postman
command_assertion

# Rust - https://www.rust-lang.org/tools/install
green_text "Installing RUST..."
curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh -s -- -y
source $HOME/.cargo/env
# Getting the propper "Run commands file to add lines to source RUST
get_rc_file
echo -ne "\n# Setup RUST environment setting in zsh - https://www.rust-lang.org/tools/install
source $HOME/.cargo/env
" >> ~/.${RC_FILE}
command_assertion
