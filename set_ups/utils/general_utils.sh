#!/bin/bash

###############################################################################
#                               General utils:                                #
###############################################################################

###############################################################################
# This are general utilities, for multiple purposes without getting into speci-
# ficity for a given set of software, add your specific software to folder
# specific utilities acording to you needs.
# It could also be easier to have a library of utilies in a file similar to this
# to record the software you use in time, so that on your next install you could
# just paste what you need into "specific_utilities.sh"
###############################################################################

# Importing text library functions to format text and asseritions
source utils/lib.sh

# Curl - https://curl.se
green_text "Installing curl..."
yes | sudo apt install curl
command_assertion

# Git - https://git-scm.com/
green_text "Installing git..."
yes | sudo apt install git
command_assertion

# Xclip - https://github.com/astrand/xclip
green_text "Installing Xclip..."
sudo apt install xclip
command_assertion

# Pip - https://pip.pypa.io/en/stable/installation/
green_text "Installing pip in python3..."
yes | sudo apt install python3-pip
command_assertion

# nvm - https://github.com/nvm-sh/nvm
green_text "Installing nvm..."
curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.39.3/install.sh | bash
command_assertion 
source ~/.zhsrc
command_assertion

# npm (from nvm) - https://github.com/nvm-sh/nvm#usage
green_text "Installing latest node version..."
nvm install 16.20.0
command_assertion

# Yarn - https://classic.yarnpkg.com/lang/en/docs/install/#debian-stable
green_text "Installing yarn..."
npm install --global yarn
command_assertion

# Snap and snap-store - https://snapcraft.io/docs/installing-snap-on-linux-mint
green_text "Installing snap..."
sudo apt install snapd
command_assertion
green_text "Installing snap-store"
sudo snap install snap-store
command_assertion

# For Okular - https://apps.kde.org/okular/ -> snap store
green_text "Installing okular..."
sudo snap install okular
command_assertion

# Telegram - https://snapcraft.io/telegram-desktop
green_text "Installing telegram..."
sudo snap install telegram-desktop
command_assertion
