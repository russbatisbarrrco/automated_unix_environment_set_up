#!/bin/bash

###############################################################################
#                              Zsh completion                                 #
###############################################################################

###############################################################################
# After installing Oh-my-zsh the current script in process is finished, so to
# finish set up(abbreviation/aliases and adding some more to .zshrc) it is
# needed to run a second script from Oh-my-zsh
###############################################################################

# Logging
NOW=$(date '+%Y-%m-%d_%H:%M:%S')
LOG_DIR='logs'
LOG_FILENAME="${LOG_DIR}/ZshCompletion-${NOW}.log"
COMPLETE_ZSH_FILE='set_ups/productivity/shell_setups/zsh/complete_zsh.sh'
bash  ${COMPLETE_ZSH_FILE} | tee -a "${LOG_FILENAME}"
echo "See logs in ${LOG_FILENAME}"

